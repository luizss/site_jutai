<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable implements Transformable
{
    use TransformableTrait;
    protected $table = 'usuarios';
    protected $fillable = ['nome', 'usuario', 'senha'];
}
