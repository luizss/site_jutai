<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rua extends Model
{
    protected $fillable = ['nome_rua', 'bairro_id'];

    public function bairro()
    {
    	return $this->belongsTo(Bairro::class);
    }
    public function ubs()
    {
        return $this->hasOne(Ubs::class);
    }
}
