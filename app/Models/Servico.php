<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $guarded = [];

    public function ubs() 
    {
        return $this->belongsToMany(Ubs::class, 'ubs_servicos');
    }
}
