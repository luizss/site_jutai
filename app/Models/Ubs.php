<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ubs extends Model
{
    protected $guarded = [];

    public function pessoas()
    {
        return $this->hasMany(Pessoa::class,'ubs_id');
    }

    public function rua()
    {
        return $this->belongsTo(Rua::class)->with('bairro');
    }

    public function servicos()
    {
        return $this->belongsToMany(Servico::class, 'ubs_servicos');
    }
}
