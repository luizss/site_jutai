<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $guarded = [];

    public function ubs()
    {
        return $this->belongsTo(Ubs::class,'ubs_id');
    }
}
