<?php

namespace App\Repositories\Pessoa;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Pessoa\PessoaRepository;
use App\Models\Pessoa;
use App\Validators\Pessoa\PessoaValidator;

/**
 * Class PessoaRepositoryEloquent.
 *
 * @package namespace App\Repositories\Pessoa;
 */
class PessoaRepositoryEloquent extends BaseRepository implements PessoaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Pessoa::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
