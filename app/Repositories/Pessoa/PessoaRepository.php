<?php

namespace App\Repositories\Pessoa;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PessoaRepository.
 *
 * @package namespace App\Repositories\Pessoa;
 */
interface PessoaRepository extends RepositoryInterface
{
    //
}
