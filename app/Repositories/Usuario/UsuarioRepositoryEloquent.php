<?php

namespace App\Repositories\Usuario;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Usuario\UsuarioRepository;
use App\Models\Usuario;

class UsuarioRepositoryEloquent extends BaseRepository implements UsuarioRepository
{
    public function model()
    {
        return Usuario::class;
    }
    
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    public function getDadosUsuario($request){
        
        $usuario = $this->model->where('usuario', $request->usuario)->where('senha', hash('sha512',$request->senha))->get();
        return $usuario;
    }
}
