<?php

namespace App\Repositories\Usuario;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsuarioRepository.
 *
 * @package namespace App\Repositories\Usuario;
 */
interface UsuarioRepository extends RepositoryInterface
{
    //
}
