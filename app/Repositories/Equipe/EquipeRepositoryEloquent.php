<?php

namespace App\Repositories\Equipe;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Equipe\EquipeRepository;
use App\Models\Equipe\Equipe;
use App\Validators\Equipe\EquipeValidator;

/**
 * Class EquipeRepositoryEloquent.
 *
 * @package namespace App\Repositories\Equipe;
 */
class EquipeRepositoryEloquent extends BaseRepository implements EquipeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Equipe::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
