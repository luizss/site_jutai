<?php

namespace App\Repositories\Equipe;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EquipeRepository.
 *
 * @package namespace App\Repositories\Equipe;
 */
interface EquipeRepository extends RepositoryInterface
{
    //
}
