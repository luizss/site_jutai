<?php

namespace App\Repositories\Ubs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UbsRepository.
 *
 * @package namespace App\Repositories\Ubs;
 */
interface UbsRepository extends RepositoryInterface
{
    //
}
