<?php

namespace App\Repositories\Ubs;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Ubs\UbsRepository;
use App\Models\Ubs;
use App\Validators\Ubs\UbsValidator;

/**
 * Class UbsRepositoryEloquent.
 *
 * @package namespace App\Repositories\Ubs;
 */
class UbsRepositoryEloquent extends BaseRepository implements UbsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Ubs::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
