<?php

namespace App\Repositories\Rua;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RuaRepository.
 *
 * @package namespace App\Repositories\Rua;
 */
interface RuaRepository extends RepositoryInterface
{
    //
}
