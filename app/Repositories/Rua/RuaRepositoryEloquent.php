<?php

namespace App\Repositories\Rua;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Rua\RuaRepository;
use App\Models\Rua;
//use App\Validators\Rua\RuaValidator;

class RuaRepositoryEloquent extends BaseRepository implements RuaRepository
{
   
    public function model()
    {
        return Rua::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
