<?php

namespace App\Repositories\Servicos;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Servicos\ServicosRepository;
use App\Models\Servico;
use App\Validators\Servicos\ServicosValidator;

/**
 * Class ServicosRepositoryEloquent.
 *
 * @package namespace App\Repositories\Servicos;
 */
class ServicosRepositoryEloquent extends BaseRepository implements ServicosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Servico::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
