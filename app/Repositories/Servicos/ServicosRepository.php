<?php

namespace App\Repositories\Servicos;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServicosRepository.
 *
 * @package namespace App\Repositories\Servicos;
 */
interface ServicosRepository extends RepositoryInterface
{
    //
}
