<?php

namespace App\Repositories\Noticia;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NoticiaRepository.
 *
 * @package namespace App\Repositories\Noticia;
 */
interface NoticiaRepository extends RepositoryInterface
{
    //
}
