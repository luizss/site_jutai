<?php

namespace App\Repositories\Bairro;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Bairro\BairroRepository;
use App\Models\Bairro;
use App\Validators\Bairro\BairroValidator;

/**
 * Class BairroRepositoryEloquent.
 *
 * @package namespace App\Repositories\Bairro;
 */
class BairroRepositoryEloquent extends BaseRepository implements BairroRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bairro::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
