<?php

namespace App\Services;

use App\Repositories\Pessoa\PessoaRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Pagination\Paginator;

class PessoaService 
{
    private $pessoaRepository;    

    public function __construct(PessoaRepositoryEloquent $pessoaRepository)
    {
        $this->pessoaRepository = $pessoaRepository;
    }

    public function listaPessoas()
    {
        $pessoas = $this->pessoaRepository->scopeQuery(function ($query){
            return $query->orderBy('nome', 'desc');
        })->paginate(10);

        return $pessoas;
    }

    public function salvar($request)
    {
    	$pessoa = $this->pessoaRepository->create($request->except('imagem'));

        $extensao = $request->imagem->extension();
        $name = uniqid(date('HisYmd'));
        $fileName = $name.".".$extensao;
        $upload = $request->imagem->move(public_path('/uploads'), $fileName);
        
        $pessoa->imagem = $fileName;
        $pessoa->save();

        return $pessoa;
    }

    public function pessoa($id)
    {
        $pessoa = $this->pessoaRepository->find($id);

        return $pessoa;
    }

    public function update($request, $id)
    {
        $pessoa = $this->pessoaRepository->update($request->except('imagem'), $id);
        if ($request->imagem) {
            $extensao = $request->imagem->extension();
            $name = uniqid(date('HisYmd'));
            $fileName = $name.".".$extensao;
            $upload = $request->imagem->move(public_path('/uploads'), $fileName);
            
            $pessoa->imagem = $fileName;
            $pessoa->save();
        }

        return $pessoa;
    }
}
