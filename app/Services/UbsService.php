<?php

namespace App\Services;

use App\Repositories\Ubs\UbsRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\Rua\RuaRepositoryEloquent;
use Illuminate\Pagination\Paginator;

class UbsService 
{
    private $ubsRepository; 
    private $ruaRepository;   

    public function __construct(UbsRepositoryEloquent $ubsRepository, RuaRepositoryEloquent $ruaRepository)
    {
        $this->ubsRepository = $ubsRepository;
        $this->ruaRepository = $ruaRepository;
    }

    public function listaUbs()
    {
        $unidades = $this->ubsRepository->scopeQuery(function($query){
            return $query->orderBy('nome_ubs','ASC');
        })->paginate(10);

    	return $unidades;
    }

    public function salvar($request)
    {
        $rua = $this->ruaRepository->find($request->rua_id);
        $data = [
            'nome_ubs' => $request->nome_ubs,
            'exibir_tela_inicial' => $request->exibir_tela_inicial,
            'url' => str_slug($request->nome_ubs),
            'numero' => $request->numero
        ];

        $ubs = $rua->ubs()->create($data);

        $extensao = $request->imagem->extension();
        $name = uniqid(date('HisYmd'));
        $fileName = $name.".".$extensao;
        $upload = $request->imagem->move(public_path('/uploads'), $fileName);
        
        $ubs->imagem = $fileName;
        $ubs->save();

        if ($request->servicos) {
            $servicosUbs = $ubs->servicos()->sync($request->servicos);
        }

    	return $ubs;
    }

    public function buscaUbs($id)
    {
    	$unidade = $this->ubsRepository->find($id);

    	return $unidade;
    }

    public function listaUbsPaginaInicial()
    {
        $unidades = $this->ubsRepository->findWhere([['exibir_tela_inicial', '=' , 's']]);

        return $unidades;
    }

    public function buscaSecretariaSaudePaginaInicial()
    {
        $secretaria = $this->ubsRepository->findWhere([['exibir_tela_inicial', '=' , 'n']])->first();

        return $secretaria;
    }

    public function update($request, $id)
    {
        $data = [
            'nome_ubs' => $request->nome_ubs,
            'exibir_tela_inicial' => $request->exibir_tela_inicial,
            'url' => str_slug($request->nome_ubs),
            'rua_id' => $request->rua_id,
            'numero' => $request->numero
        ];

        $ubs = $this->ubsRepository->update($data, $id);

        if (isset($request->imagem)) {
            $extensao = $request->imagem->extension();
            $name = uniqid(date('HisYmd'));
            $fileName = $name.".".$extensao;
            $upload = $request->imagem->move(public_path('/uploads'), $fileName);
            
            $ubs->imagem = $fileName;
            $ubs->save();
        }
        
        if ($request->servicos) {
            $servicosUbs = $ubs->servicos()->sync($request->servicos);
        }

    	return $ubs;
    }

    public function exibirInformacoesUnidade($unidade)
    {
        $unidade = $this->ubsRepository->findWhere([['url', '=', $unidade]])->first();
        $funcionarios = $unidade->pessoas;
        $servicos = $unidade->servicos; 
        $rua = $unidade->rua;
        $bairro = $unidade->rua->bairro->nome_bairro;
        $data = [
            'unidade' => $unidade,
            'funcionarios' => $funcionarios,
            'servicos' => $servicos,
            'rua' => $rua,
            'bairro' => $bairro
        ];

        return $data;
    }

    public function pesquisaUnidades($pesquisa) 
    {
        $unidades = $this->ubsRepository->findWhere([['nome_ubs','LIKE', '%'.$pesquisa.'%']]);

        return response()->json($unidades, 200);
    }
}