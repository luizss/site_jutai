<?php

namespace App\Services;

use App\Repositories\Rua\RuaRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Pagination\Paginator;

class RuasService 
{
    private $ruaRepository;    

    public function __construct(RuaRepositoryEloquent $ruaRepository)
    {
        $this->ruaRepository = $ruaRepository;
    }

    public function listaRuas()
    {
        $ruas = $this->ruaRepository->scopeQuery(function($query) {
            return $query->orderBy('nome_rua','ASC');
        })->paginate(10);

        return $ruas;
    }

    public function salvar($request)
    {
    	$salvar = $this->ruaRepository->create($request->all());

    	return $salvar;
    }

    public function rua($id)
    {
        $rua = $this->ruaRepository->find($id);

        return $rua;
    }

    public function update($request, $id)
    {
        $update = $this->ruaRepository->update($request->all(), $id);

        return $update;
    }
}