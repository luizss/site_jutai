<?php

namespace App\Services;

use App\Repositories\Bairro\BairroRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;


class BairrosService 
{
    private $bairroRepository;    

    public function __construct(BairroRepositoryEloquent $bairroRepository)
    {
        $this->bairroRepository = $bairroRepository;
    }
    public function salvar($request)
    {
        $salvar = $this->bairroRepository->create($request->all());
        return $salvar;
    }

    public function update($request, $id)
    {
        $bairro = $this->bairroRepository->update($request->all(), $id);
        return $bairro;
    }

    public function lista_bairros()
    {
        $bairros = $this->bairroRepository->all()->pluck('nome_bairro', 'id');

        return $bairros;
    }
} 