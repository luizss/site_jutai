<?php

namespace App\Services;

use App\Repositories\Noticia\NoticiaRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Pagination\Paginator;

class NoticiaService 
{
    private $noticiaRepository;    

    public function __construct(NoticiaRepositoryEloquent $noticiaRepository)
    {
        $this->noticiaRepository = $noticiaRepository;
    }

    public function listaNoticias()
    {
        $noticias = $this->noticiaRepository->scopeQuery(function($query) {
            return $query->orderBy('id','DESC');
        })->paginate(10);

        return $noticias;
    }

    public function salvar($request)
    {
        $url = str_slug($request->titulo_noticia);
        $data = [
            'titulo_noticia' => $request->titulo_noticia,
            'conteudo_noticia' => $request->conteudo_noticia,
            'url' => $url
        ];
        $noticia = $this->noticiaRepository->create($data);
        
        $extensao = $request->image_noticia->extension();
        $name = uniqid(date('HisYmd'));
        $fileName = $name.".".$extensao;
        // $upload = public_path('uploads/'.$fileName);
        $upload = $request->image_noticia->move(public_path('/uploads'), $fileName);
        
        $noticia->image_noticia = $fileName;
    	$noticia->save();

    	return $noticia;
    }

    public function noticia($id)
    {
        $noticia = $this->noticiaRepository->find($id);

        return $noticia;
    }
    public function exibirNoticia($noticia){
        $noticia = $this->noticiaRepository->findWhere([['url', '=' , $noticia]])->first();

        return $noticia;
    }

    public function update($request, $id)
    {
        $url = str_slug($request->titulo_noticia);
        
        if ($request->image_noticia) {
            $extensao = $request->image_noticia->extension();
            $name = uniqid(date('HisYmd'));
            $fileName = $name.".".$extensao;
            // $upload = public_path('uploads/'.$fileName);
            $upload = $request->image_noticia->move(public_path('/uploads'), $fileName);
            $data = [
                'titulo_noticia' => $request->titulo_noticia,
                'conteudo_noticia' => $request->conteudo_noticia,
                'image_noticia' => $fileName,
                'url' => $url
            ];
            
        } else {
            $data = [
                'titulo_noticia' => $request->titulo_noticia,
                'conteudo_noticia' => $request->conteudo_noticia,
                'url' => $url
            ];
        }
        
        $update = $this->noticiaRepository->update($data, $id);
        
        return $update;
    }

    public function noticias()
    {
        $noticias = $this->noticiaRepository->scopeQuery(function($query) {
            return $query->orderBy('created_at','DESC')->take(5);
        })->all();

        return $noticias;
    }

    public function destroy($id) {
        $noticias = $this->noticiaRepository->delete($id);
    }
}
