<?php

namespace App\Services;

use App\Repositories\Servicos\ServicosRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Illuminate\Pagination\Paginator;

class ServicosService 
{
    private $servicosRepository;

    public function __construct(ServicosRepositoryEloquent $servicosRepository)
    {
        $this->servicosRepository = $servicosRepository;
    }

    public function servicosAll()
    {
        $servicos = $this->servicosRepository->scopeQuery(function ($query) {
            return $query->orderBy('nome', 'ASC');
        })->paginate(10);

        return $servicos;
    }

    public function salvar($request) {
        $salvar = $this->servicosRepository->create($request->all());

        return $salvar;
    }

    public function editar($id) 
    {
        $servico = $this->servicosRepository->find($id);
        return $servico;
    }

    public function update($request, $id)
    {
        $update = $this->servicosRepository->update($request->all(), $id);

        return $update;
    }

    public function listaTodosServicos() {
        $servicos = $this->servicosRepository->scopeQuery(function ($query){
            return $query->orderBy('nome', 'ASC');
        })->all()->pluck('nome', 'id');

        return $servicos;
    }
    public function servicosPaginaInicial() 
    {
        $servicos = $this->servicosRepository->scopeQuery(function ($query){
            return $query->take(3);
        })->get();

        return $servicos;
    }
}