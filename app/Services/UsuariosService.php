<?php

namespace App\Services;

use App\Repositories\Usuario\UsuarioRepositoryEloquent;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class UsuariosService 
{
    private $usuarioRepository;    

    public function __construct(UsuarioRepositoryEloquent $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }
    public function doLogin($request) 
    {
        $usuario = $this->usuarioRepository->findWhere(['usuario' => $request->usuario, 'senha' => hash('sha512', $request->senha)])->first();
        // dd($usuario);
        return $usuario;
    }
}