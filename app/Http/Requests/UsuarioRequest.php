<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nome' => 'required',
            'usuario' => 'required',
            'senha' => 'required'  
        ];
    }
    public function messages()
    {
        return [
            '*.required' => 'O :attribute é um campo obrigatório'
        ];
    }
}
