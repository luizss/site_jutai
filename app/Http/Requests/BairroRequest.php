<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BairroRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome_bairro' => 'required'
        ];
    }
    public function messages(){
        return [
            'nome_bairro.required' => 'O :attribute é um campo obrigatório'
        ];
    }
}

