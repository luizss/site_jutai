<?php

namespace App\Http\Middleware;

use Closure;

class VerificaPermissao
{
    public function handle($request, Closure $next)
    {
        if (!\Auth::check()){
            return redirect()->route('inicio')->withErrors(['Sessão expirada!']);
        }
        return $next($request);
    }
}
