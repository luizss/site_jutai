<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ServicosService;
use App\Http\Requests\ServicosRequest;

class ServicosController extends Controller
{
    private $servicosService;

    public function __construct(ServicosService $servicosService)
    {
        $this->servicosService = $servicosService;
    }

    public function index()
    {
        $servicos = $this->servicosService->servicosAll();
        
        return view('restrito.servicos.index', compact('servicos'));
    }

    public function create()
    {
        return view('restrito.servicos.form');
    }

    public function store(ServicosRequest $request)
    {
        $salvar = $this->servicosService->salvar($request);

        if ($salvar) {
            session()->flash('success', 'Serciço cadastrado com sucesso!');

            return redirect()->route('servicos.index');
        }
    }

    public function edit($id)
    {
        $servico = $this->servicosService->editar($id);

        return view('restrito.servicos.form', compact('servico'));
    }

    public function update(ServicosRequest $request, $id)
    {
        $atualizar = $this->servicosService->update($request, $id);

        if ($atualizar) {
            session()->flash('success', 'Cadastro atualizado com sucesso!');

            return redirect()->route('servicos.index');
        }
    }

    public function destroy($id)
    {

    }
}
