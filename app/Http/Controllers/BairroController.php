<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Bairro\BairroRepositoryEloquent;
use App\Http\Requests\BairroRequest;
use App\Services\BairrosService;
use Illuminate\Pagination\Paginator;

class BairroController extends Controller
{
    private $bairroRepository;
    private $bairroService;
    public function __construct(
        BairroRepositoryEloquent $bairroRepository, 
        BairrosService $bairroService
    )
    {
        $this->bairroRepository = $bairroRepository;
        $this->bairroService = $bairroService;
    }

    public function index()
    {
        $bairros = $this->bairroRepository->paginate(10);
	    return view('restrito.bairros.index', compact('bairros'));
    }

    public function create()
    {
        return view('restrito.bairros.form');
    }

    public function store(BairroRequest $request)
    {
        $salvar = $this->bairroService->salvar($request);
        if ($salvar) {
            \Session::flash('success', 'Salvo com sucesso');
            return redirect()->route('bairros.index');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bairro = $this->bairroRepository->find($id);
        return view('restrito.bairros.form', compact('bairro'));        
    }

    public function update(BairroRequest $request, $id)
    {
        $update = $this->bairroService->update($request, $id);
        if ($update) {
            \Session::flash('success', 'Dados atualizados com sucesso!');
            return redirect()->route('bairros.index');
        }
    }

    public function destroy($id)
    {
        //
    }
}
