<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UsuariosService;
use Auth;
class LoginController extends Controller
{
    private $usuarioService;
    public function __construct(UsuariosService $usuarioService)
    {
        $this->usuarioService = $usuarioService;
    }
    public function doLogin(Request $request)
    {
        // dd($request);
        $usuario = $this->usuarioService->doLogin($request);
        if (!$usuario) {
            return redirect()->route('inicio')->withErrors('Usuário ou senha inválidos!');
        } else {
            // dd("entrou");
            if (!Auth::loginUsingId($usuario->id, false)) {
                return redirect()->route('inicio')->withErrors('Não foi possível realizar login');
            } else {
                // dd("entrou");
                return redirect()->route('painel.controle');
            }
        }
    }

    public function doLogout() {
        Auth::logout();
        return redirect()->route('inicio');
    }
}
