<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PessoaService;
use App\Services\UbsService;

class PessoasController extends Controller
{
    private $pessoaService;
    private $ubsService;

    public function __construct(PessoaService $pessoaService, UbsService $ubsService)
    {
        $this->pessoaService = $pessoaService;
        $this->ubsService = $ubsService;
    }

    public function index()
    {
        $pessoas = $this->pessoaService->listaPessoas();

        return view('restrito.pessoas.index', compact('pessoas'));
    }
    
    public function create()
    {
        $cargos = $this->cargos();
        $ubs = $this->ubsService->listaUbs()->pluck('nome_ubs', 'id');
        return view('restrito.pessoas.form', compact('ubs', 'cargos'));
    }

    public function store(Request $request)
    {
        $salvar = $this->pessoaService->salvar($request);

        if ($salvar) {
            \Session::flash('success','Pessoa cadastrada com sucesso');
            return redirect()->route('pessoas.index');
        }
    }

    public function edit($id)
    {
        $pessoa = $this->pessoaService->pessoa($id);
        $cargos = $this->cargos();
        $ubs = $this->ubsService->listaUbs()->pluck('nome_ubs', 'id');

        return view('restrito.pessoas.form', compact('ubs', 'cargos', 'pessoa'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $update = $this->pessoaService->update($request, $id);
        
        if ($update) {
            \Session::flash('success','Registro alterado com sucesso');
            return redirect()->route('pessoas.index');
        }
    }

    public function destroy($id)
    {
        
    }

    public function cargos()
    {
        $cargos = [
            'Auxiliar Administrativo' => 'Auxiliar Administrativo',
            'Digitador (a)' => 'Digitador (a)',
            'Coordenador da Vigilância sanitária' => 'Coordenação da Vigilância sanitária',
            'Coordenador de Projetos' => 'Coordenador de Projetos',
            'Coordenador de Sistema da Informação' => 'Coordenador de Sistema da Informação',
            'Coordenador (a) da Atenção Básica' => 'Coordenador (a) da Atenção Básica',
            'Motorista' => 'Motorista',
            'Recepcionista' => 'Recepcionista',
            'Secretário (a) de Saúde' => 'Secretário (a) de Saúde',
            'Serviços Gerais' => 'Serviços Gerais',
            'Técnico Bolsa família' => 'Técnico Bolsa família'
        ];

        return $cargos;
    }
}
