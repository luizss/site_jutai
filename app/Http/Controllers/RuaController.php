<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Rua\RuaRepositoryEloquent;
use App\Services\RuasService;
use App\Services\BairrosService;

class RuaController extends Controller
{
    private $ruaRepository;
    private $ruasService;
    private $bairrosService;

    public function __construct(RuaRepositoryEloquent $ruaRepository, RuasService $ruasService, BairrosService $bairrosService)
    {
        $this->ruaRepository = $ruaRepository;
        $this->ruasService = $ruasService;
        $this->bairrosService = $bairrosService;
    }

    public function index()
    {
        $ruas = $this->ruasService->listaRuas();

        return view('restrito.ruas.index', compact('ruas'));
    }

    public function create()
    {
        $bairros = $this->bairrosService->lista_bairros();

        return view('restrito.ruas.form', compact('bairros'));
    }

    public function store(Request $request)
    {
        $salvar = $this->ruasService->salvar($request);

        if($salvar){
            \Session::flash('success','Rua cadastrada com sucesso');
            return redirect()->route('ruas.index');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
         $rua = $this->ruasService->rua($id);   
         $bairros = $this->bairrosService->lista_bairros();
         
         return view('restrito.ruas.form', compact('rua', 'bairros'));
    }

    public function update(Request $request, $id)
    {
        $update = $this->ruasService->update($request, $id);

        if ($update) {
            \Session::flash('success', 'Cadastro atualizado com sucesso');

            return redirect()->route('ruas.index');
        }
    }

    public function destroy($id)
    {
        //
    }
}
