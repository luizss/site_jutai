<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UbsService;
use App\Services\PessoaService;
use App\Services\NoticiaService;
use App\Services\ServicosService;

class InicioController extends Controller
{
    private $noticiaService;
    private $pessoaService;
    private $ubsService;
    private $servicosService;

    public function __construct(
        NoticiaService $noticiaService,
        PessoaService $pessoaService,
        UbsService $ubsService,
        ServicosService $servicosService
    )
    {
        $this->noticiaService = $noticiaService;
        $this->pessoaService = $pessoaService;
        $this->ubsService = $ubsService;
        $this->servicosService = $servicosService;
    }
    public function index()
    {
        $page = 'Login';
        return view('inicio', compact('page'));
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function painel()
    {
        return view('dashboard');
    }
    
    public function site()
    {
        $noticias = $this->noticiaService->noticias();
        $pessoas = $this->pessoaService->listaPessoas();
        $unidades = $this->ubsService->listaUbsPaginaInicial();
        $secretaria = $this->ubsService->buscaSecretariaSaudePaginaInicial();
        $servicos = $this->servicosService->servicosPaginaInicial();
        $page = 'Prefeitura de Jutaí :: Home';
        
        return view('welcome', compact('page', 'noticias', 'pessoas', 'unidades', 'secretaria', 'servicos'));
    }

    public function buscaUbs()
    {
        $secretaria = $this->ubsService->buscaSecretariaSaudePaginaInicial();
        $page = 'Prefeitura de Jutaí :: Unidades';
        return view('site.unidades.show', compact('secretaria', 'page'));
    }
}
