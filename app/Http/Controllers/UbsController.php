<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UbsRequest;
use App\Repositories\Ubs\UbsRepositoryEloquent;
use App\Services\UbsService;
use App\Services\RuasService;
use App\Services\ServicosService;

class UbsController extends Controller
{
    private $ubsRepository;
    private $ubsService;
    private $ruasService;
    private $servicosService;

    public function __construct(UbsRepositoryEloquent $ubsRepository, UbsService $ubsService, RuasService $ruasService, ServicosService $servicosService)
    {
        $this->ubsRepository = $ubsRepository;
        $this->ubsService = $ubsService;
        $this->ruasService = $ruasService;
        $this->servicosService = $servicosService;
    }

    public function index()
    {
        $unidades = $this->ubsService->listaUbs();

        return view('restrito.ubs.index', compact('unidades'));
    }

    public function create()
    {
        $arrayRuas = array();
        $ruas = $this->ruasService->listaRuas();
        foreach ($ruas as $rua) {
            $arrayRuas[$rua->id] = $rua->nome_rua . " - " . $rua->bairro->nome_bairro;
        }
        
        $servicos = $this->servicosService->listaTodosServicos();

        return view('restrito.ubs.form', compact('arrayRuas', 'servicos'));
    }

    public function store(UbsRequest $request)
    {
        $salvar = $this->ubsService->salvar($request);

        if ($salvar) {
            \Session::flash('success', 'Salvo com sucesso');
            return redirect()->route('ubs.index');
        }
    }

    public function show($unidade)
    {
        $unidade = $this->ubsService->exibirInformacoesUnidade($unidade);
    
        return response()->json($unidade, 200);
    }

    public function edit($id)
    {
        $arrayRuas = array();
        $ruas = $this->ruasService->listaRuas();
        foreach ($ruas as $rua) {
            $arrayRuas[$rua->id] = $rua->nome_rua . " - " . $rua->bairro->nome_bairro;
        }

        $unidade = $this->ubsService->buscaUbs($id);
        $servicos = $this->servicosService->listaTodosServicos();

        return view('restrito.ubs.form', compact('unidade', 'arrayRuas', 'servicos'));
    }

    public function update(UbsRequest $request, $id)
    {
        $update = $this->ubsService->update($request, $id);

        if ($update) {
            \Session::flash('success', 'Cadastro atualizado');
            return redirect()->route('ubs.index');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function searchUnidade($pesquisa) 
    {
        $unidade = $this->ubsService->pesquisaUnidades($pesquisa);

        return $unidade;
    }
}
