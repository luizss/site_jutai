<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NoticiaService;
use App\Services\UbsService;
use App\Services\ServicosService;

class NoticiasController extends Controller
{
    private $noticiaService;
    private $ubsService;
    private $servicosService;

    public function __construct(NoticiaService $noticiaService, UbsService $ubsService, ServicosService $servicosService)
    {
        $this->noticiaService = $noticiaService;
        $this->ubsService = $ubsService;
        $this->servicosService = $servicosService;
    }

    public function index()
    {
        $noticias = $this->noticiaService->listaNoticias();

        return view('restrito.noticias.index', compact('noticias'));
    }

    public function create()
    {
        return view('restrito.noticias.form');
    }

    public function store(Request $request)
    {
        $salvar = $this->noticiaService->salvar($request);

        if($salvar){
            \Session::flash('success','Notícia cadastrada com sucesso');
            return redirect()->route('noticias.index');
        }
    }

    public function show($noticia)
    {
        $noticia = $this->noticiaService->exibirNoticia($noticia);
        $secretaria = $this->ubsService->buscaSecretariaSaudePaginaInicial();
        $servicos = $this->servicosService->servicosPaginaInicial();
        $page = $noticia->titulo_noticia;

        return view('site.noticias.show', compact('noticia', 'page', 'secretaria', 'servicos'));
    }

    public function edit($id)
    {
         $noticia = $this->noticiaService->noticia($id);   
         
         return view('restrito.noticias.form', compact('noticia'));
    }

    public function update(Request $request, $id)
    {
        $update = $this->noticiaService->update($request, $id);

        if ($update) {
            \Session::flash('success', 'Cadastro atualizado com sucesso');

            return redirect()->route('noticias.index');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function exibirTodasNotícias()
    {
        $noticias = $this->noticiaService->listaNoticias();
        $secretaria = $this->ubsService->buscaSecretariaSaudePaginaInicial();
        $servicos = $this->servicosService->servicosPaginaInicial();
        $page = 'Prefeitura de Jutaí :: Notícias';

        return view('site.noticias.index', compact('secretaria', 'page', 'noticias', 'servicos'));
    }
}
