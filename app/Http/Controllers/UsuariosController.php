<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UsuarioRequest;
use App\Repositories\Usuario\UsuarioRepositoryEloquent;
use App\Services\UsuariosService;
class UsuariosController extends Controller
{
    protected $usuarioRepository;
    protected $usuarioService;
    public function __construct(UsuarioRepositoryEloquent $usuarioRepository, UsuariosService $usuarioService)
    {
        $this->usuarioRepository = $usuarioRepository;
        $this->usuarioService  = $usuarioService;
    }

    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $usuarios = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $usuarios,
            ]);
        }

        return view('usuarios.index', compact('usuarios'));
    }

    public function store(UsuarioRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $usuario = $this->repository->create($request->all());

            $response = [
                'message' => 'Usuario created.',
                'data'    => $usuario->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function show($id)
    {
        $usuario = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $usuario,
            ]);
        }

        return view('usuarios.show', compact('usuario'));
    }

    public function edit($id)
    {
        $usuario = $this->repository->find($id);

        return view('usuarios.edit', compact('usuario'));
    }

    public function update(UsuarioRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $usuario = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Usuario updated.',
                'data'    => $usuario->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Usuario deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Usuario deleted.');
    }
}
