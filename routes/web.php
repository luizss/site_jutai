<?php

Route::get('/', 'InicioController@site')->name('homepage');
Route::get('/entrar', 'InicioController@index')->name('inicio');
Route::post('/fazer-login', 'LoginController@doLogin')->name('login.usuario');
Route::post('/sair','LoginController@doLogout')->name('fazer.logout');
Route::group(['prefix' => 'restrito', 'middleware' => 'verifica-permissao'], function () {
    Route::get('/','InicioController@painel')->name('painel.controle');
    Route::resource('bairros','BairroController');
    Route::resource('ruas', 'RuaController');
    Route::resource('ubs', 'UbsController');
    Route::resource('noticias', 'NoticiasController');
    Route::resource('pessoas', 'PessoasController');
    Route::resource('servicos', 'ServicosController');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('noticia/{noticia}', 'NoticiasController@show')->name('noticia.show');
Route::get('noticias/ver-noticias','NoticiasController@exibirTodasNotícias')->name('noticias.all');
Route::get('unidade/{ubs}','UbsController@show')->name('unidade.show');
Route::get('busca-ubs','InicioController@buscaUbs')->name('ubs.seach');
Route::get('search/{unidade}', 'UbsController@searchUnidade')->name('search.unidade');
