@extends('layouts.app')
@section('content')
    @include('layouts.carousel')
    
    @include('layouts.pessoas')
    
    @include('layouts.ubs')
    
    @include('layouts.footer')
@endsection
