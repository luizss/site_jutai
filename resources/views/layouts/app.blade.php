<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <style>
        .wrapper{
            width: 100%;
            height: auto;
            overflow: hidden;
        }
        #pessoas{
            margin-top: 30px;
        }
        #titulo-pagina-noticia {
            padding-top: 20px;
            text-align: justify !important;
        }
        #conteudo-noticia{
            text-align: justify;
            text-indent: 40px;
        }
        #conteudo-ubs{
            text-align: justify;
        }
        #updated_at{
            text-indent: 0px;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}"><img id="logo" src="{{ asset('image/logo.png') }}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('inicio') }}">Login</a>
                        </li>                    
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('noticias.all') }}">Notícias</a>
                        </li>                    
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('ubs.seach') }}">UBS</a>
                        </li>                    
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')   
    </div>
    <script>
        @yield('scripts')
    </script>
</body>
</html>
