<div class="rodape">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4 class="text-center">{{ @$secretaria->nome_ubs }}</h4>
                <p class="text-center">Endereço: {{ @$secretaria->rua->nome_rua }}, nº {{ @$secretaria->numero }} - {{ @$secretaria->rua->bairro->nome_bairro }} - Jutaí - AM</p>
            </div>
            <div class="col-md-4">
                <h4 class="text-center">Redes Sociais</h4>
                <div class="row">
                    <div class="col text-center" >
                        <i class="fab fa-facebook"></i> <a href="">facebook</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="text-center">Serviços oferecidos</h4>
                @forelse ($servicos as $servico) 
                    <div class="text-center">{{ $servico->nome }}</div>
                @empty
                @endforelse
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center">{{ date('Y') }} - Luiz Souza</h5>
            </div>
        </div>
    </div>
</div>