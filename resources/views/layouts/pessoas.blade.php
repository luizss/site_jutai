<div class="container" id="pessoas">
    <h2 class="text-center">Nossa equipe</h2>
    <div class="wrapper">
        <div class="owl-carousel owl-theme">
            @foreach ($pessoas as $pessoa)
                <div class="item">
                    <div class="card">
                        <img src="{{ asset('uploads/'.$pessoa->imagem) }}" id="card-pessoa" class="card-img-top" alt="...">
                        <div class="card-body pessoas">
                            <b class="card-title">{{ $pessoa->nome }}</b>
                            <p class="card-text">
                                <small>
                                    {{ $pessoa->cargo }} - {{ $pessoa->ubs->nome_ubs }}
                                </small>    
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<script>
    $('.owl-carousel').owlCarousel({
    loop:true,
    autoplay: true,
    nav : false,
    dots: true,
    margin:10,
    // pagination: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
</script>
{{-- <div class="container">
    <div class="row">
        <h2 class="text-center">
            Nossa equipe
        </h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{ asset('images/facebook-pessoas.jpg') }}" id="img-pessoa" alt="">
                        <div class="col-md-12">
                            <h5>Maria</h5>
                            <h6>Gerente de projetos</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{ asset('images/facebook-pessoas.jpg') }}" id="img-pessoa" alt="">
                        <div class="col-md-12">
                            <h5>Maria</h5>
                            <h6>Gerente de projetos</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{ asset('images/facebook-pessoas.jpg') }}" id="img-pessoa" alt="">
                        <div class="col-md-12">
                            <h5>Maria</h5>
                            <h6>Gerente de projetos</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img src="{{ asset('images/facebook-pessoas.jpg') }}" id="img-pessoa" alt="">
                        <div class="col-md-12">
                            <h5>Maria</h5>
                            <h6>Gerente de projetos</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
                <a href="#" class="btn btn-primary pull-right">Ver mais&nbsp;&nbsp;<i class="fas fa-plus"></i></a>
        </div>
    </div>
</div> --}}