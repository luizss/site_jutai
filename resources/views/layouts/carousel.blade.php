<div id="myCarousel" class="carousel slide">
    @php 
        $indicators = 0;
        $items = 0; 
    @endphp
    <ol class="carousel-indicators">
        @foreach ($noticias as $noticia)
            <li data-target="#myCarousel" data-slide-to="{{ $indicators }}" @if ($indicators == 0) class="active" @endif></li>
            @php $indicators++; @endphp
            {{-- <li data-target="#myCarousel" data-slide-to="1"></li> --}}
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach ($noticias as $noticia)
        {{-- {{ dd($noticia->titulo) }} --}}
            <div class="carousel-item @if ($items == 0) active @endif">
                <img class="image-carousel" src="{{ asset('uploads/'.$noticia->image_noticia) }}" alt="...">
                <div class="carousel-caption d-block d-md-block">
                    <h3 id="titulo-noticia">{{ $noticia->titulo_noticia }}</h3>
                    <a href="{{ route('noticia.show', $noticia->url) }}" id="botao-noticia" class="btn btn-primary" title="Clique para visualizar"><i class="fas fa-eye"></i> Ver notícia</a>
                </div>
            </div>
            @php $items++; @endphp
        @endforeach
        {{-- <div class="carousel-item">
            <img class="image-carousel" src="{{ asset('images/hotel2.jpg') }}" alt="...">
            <div class="carousel-caption d-block d-md-block">
                <h3 id="titulo-noticia">Teste</h3>
                <a href="" id="botao-noticia" class="btn btn-primary">Ver notícia</a>
            </div>
        </div> --}}
        
    </div>

    <a class="carousel-control left" href="#myCarousel"
       data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel"
       data-slide="next">&rsaquo;</a>
</div>
<script>
    $(document).ready(function(){
        $("#myCarousel").carousel({interval: 5000});
    });
</script>