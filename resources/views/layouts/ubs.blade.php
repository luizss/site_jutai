<div class="container" id="pessoas">
    <h2 class="text-center">Unidades Básicas de saúde</h2>
    <div class="wrapper">
        <div class="owl-carousel owl-theme">
            @foreach ($unidades as $unidade)
                {{-- <a id="item-ubs" href="{{ route('unidade.show', $unidade->url) }}"> --}}
                <a data-toggle="modal" data-target="#unidade" data-unidade="{{ $unidade->url }}">
                    <div class="item">
                        <div class="card" style="width: 18rem;">
                            <img src="{{ asset('uploads/'.$unidade->imagem) }}" id="img-ubs" class="card-img-top" alt="...">
                            <div class="card-body">
                                <b class="card-title">{{ $unidade->nome_ubs }}</b>
                                <p class="card-text"><small>{{ $unidade->rua->nome_rua }}, {{ $unidade->numero }} - {{ $unidade->rua->bairro->nome_bairro }}</small></p>
                            </div>
                        </div>
                        {{-- <span class="view-info"><i class="fas fa-eye"></i></span> --}}
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    @include('site.unidades.modal-detalhes')
<script>
    $('.owl-carousel').owlCarousel({
    loop:true,
    autoplay: 4000,
    nav : false,
    dots: true,
    margin:10,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
$('#unidade').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var url_unidade = button.data('unidade') 
  var modal = $(this);
  var dados_servicos = '';
  var dados_funcionarios = '';
  var dados_unidade = '';
  $('#funcionarios').empty();
  $('#servicos').empty();
  $('#dados-unidade').empty();

  $.ajax({
          url : "unidade/"+url_unidade,
          type : 'GET',
          contentType: "application/json",
          dataType: "json",
          success: function(response){
            
            document.getElementById('unidadeLabel').innerHTML = '<i class="fas fa-info-circle"></i> '+response.unidade.nome_ubs;
            
            dados_unidade += '<h4><i class="fas fa-map-marker-alt"></i> Endereço:</h4>'; 
            dados_unidade += '<p>'+ response.rua.nome_rua + ', ' + response.unidade.numero + ' - ' + response.bairro + '</p>'
            $('#dados-unidade').append(dados_unidade);


            if (Object.keys(response.funcionarios).length > 0) {
              dados_funcionarios += '<h4><i class="fas fa-users"></i> Funcionários da unidade</h4>';
            }
            
            $.each(response.funcionarios, function(key, value){
               dados_funcionarios += '<p>' + value.nome + ': ' + value.cargo + '</p>'  
            });
            
            $('#funcionarios').append(dados_funcionarios);

            if (Object.keys(response.servicos).length > 0) {
              dados_servicos += '<h4><i class="fas fa-user-md"></i> Serviços disponíveis</h4>';
            } 
            
            $.each(response.servicos, function( chave, valor ) {
                dados_servicos += '<p><b>' + valor.nome + '</b> ' + valor.descricao + '</p>';
            });
            
            $('#servicos').append(dados_servicos);
          },
          error: function() {
              
          }
     });
})
</script>
