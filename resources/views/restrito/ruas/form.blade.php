@extends('adminlte::page')
@section('title', 'Restrito :: Ruas')

@section('content_header')
    <h1>Formulario de cadastro de ruas</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($rua))
                        {!! Form::model($rua,['method' => 'PATCH','route' => ['ruas.update',$rua->id]]) !!}
                    @else
                        {!! Form::open(['route' => 'ruas.store']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('nome_rua', 'Bairro') !!}
                            {!! Form::text('nome_rua', null, ['class' => 'form-control', 'placeholder' => 'Nome da rua']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('bairro_id', 'Bairro') !!}
                            {!! Form::select('bairro_id', $bairros, null, ['class' => 'form-control', 'placeholder' => 'Selecione o bairro']) !!}
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop