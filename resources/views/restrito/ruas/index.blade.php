@extends('adminlte::page')
@section('title', 'Restrito :: ruas')

@section('content_header')
    <h1>Lista de Ruas</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('ruas.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Nome da rua</th>
                                <th>Bairro</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($ruas as $rua)
                                <tr>
                                    <td>
                                        <a href="{{ route('ruas.edit', $rua->id) }}" class="btn btn-default btn-sm">Editar</a>
                                    </td>        
                                    <td>{{ $rua->nome_rua }}</td>
                                    <td>{{ $rua->bairro->nome_bairro }}</td>
                                @empty
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $ruas->links() }}
                </div>
            </div>
        </div>
    </div>
@stop