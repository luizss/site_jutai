@extends('adminlte::page')
@section('title', 'Restrito :: UBS')

@section('content_header')
    <h1>Lista de Unidades Básicas de Saúde - UBS</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('ubs.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Nome da Unidade</th>
                                <th>Endereço</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($unidades as $unidade)
                                <tr>
                                    <td>
                                        <a href="{{ route('ubs.edit', $unidade->id) }}" class="btn btn-default">Editar</a>
                                    </td>        
                                    <td>{{ $unidade->nome_ubs }}</td>
                                    <td>{{ $unidade->rua->nome_rua }}, {{ $unidade->numero }} - {{ $unidade->rua->bairro->nome_bairro }}</td>
                                @empty
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $unidades->links() }}
                </div>
            </div>
        </div>
    </div>
@stop