@extends('adminlte::page')
@section('title', 'Restrito :: UBS')

@section('content_header')
    <h1>Formulario de cadastro de Unidades Básicas de Saúde</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($unidade))
                        {!! Form::model($unidade,['method' => 'PATCH','route' => ['ubs.update',$unidade->id], 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['route' => 'ubs.store', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('nome_ubs', 'Nome da lotação') !!}
                            {!! Form::text('nome_ubs', null, ['class' => 'form-control', 'placeholder' => 'Nome do Unidade']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('exibir_tela_inicial', 'Exibir na tela inicial?') !!}
                            <div class="row">
                                <div class="form-group col-md-1">
                                    {!! Form::radio('exibir_tela_inicial', 's', null) !!} Sim
                                </div>
                                <div class="form-group col-md-11">
                                    {!! Form::radio('exibir_tela_inicial', 'n', null) !!} Não
                                </div>
                            </div>    
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('rua_id', 'Rua') !!}
                            {!! Form::select('rua_id', $arrayRuas, null, ['class' => 'form-control', 'placeholder' => 'Escolha a rua']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('numero', 'Número') !!}
                            {!! Form::text('numero', null, ['class' => 'form-control', 'placeholder' => 'Número']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('servicos','Selecione os serviços') !!}
                            {!! Form::select('servicos[]',$servicos, null, ['class' => 'form-control servicos', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('imagem', 'Imagem da unidade') !!}
                            {!! Form::file('imagem', null) !!}
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop