@extends('adminlte::page')
@section('title', 'Restrito :: bairros')

@section('content_header')
    <h1>Formulario de cadastro de bairros</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($bairro))
                        {!! Form::model($bairro,['method' => 'PATCH','route' => ['bairros.update',$bairro->id]]) !!}
                    @else
                        {!! Form::open(['route' => 'bairros.store']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('nome_bairro', 'Bairro') !!}
                            {!! Form::text('nome_bairro', null, ['class' => 'form-control', 'placeholder' => 'Nome do bairro']) !!}
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop