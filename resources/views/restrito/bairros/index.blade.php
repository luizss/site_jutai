@extends('adminlte::page')
@section('title', 'Restrito :: bairros')

@section('content_header')
    <h1>Lista de Bairros</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('bairros.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Nome do bairro</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($bairros as $bairro)
                                <tr>
                                    <td>
                                        <a href="{{ route('bairros.edit', $bairro->id) }}" class="btn btn-default btn-sm">Editar</a>
                                    </td>        
                                    <td>{{ $bairro->nome_bairro }}</td>
                                @empty
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $bairros->links() }}
                </div>
            </div>
        </div>
    </div>
@stop