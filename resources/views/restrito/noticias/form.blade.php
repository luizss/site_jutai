@extends('adminlte::page')
@section('title', 'Restrito :: notícias')

@section('content_header')
    <h1>Formulario de cadastro de nottícias</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($noticia))
                        {!! Form::model($noticia,['method' => 'PATCH','route' => ['noticias.update',$noticia->id], 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['route' => 'noticias.store', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('titulo_noticia', 'Título') !!}
                            {!! Form::text('titulo_noticia', null, ['class' => 'form-control', 'placeholder' => 'Título da notícia']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('conteudo_noticia', 'Conteúdo') !!}
                            {!! Form::textarea('conteudo_noticia', null, ['class' => 'form-control', 'placeholder' => 'Conteúdo da notícia']) !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('image_noticia', 'Imagem') !!}
                            {!! Form::file('image_noticia', null, ['class' => 'form-control']) !!}
                            @if (isset($noticia))
                                <img class="img img-responsive" src="{{ asset('uploads/'.$noticia->image_noticia) }}" alt="">
                            @endif
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        window.onload = function()  {
            CKEDITOR.replace( 'conteudo_noticia' );
        };
    </script>
@endpush