@extends('adminlte::page')
@section('title', 'Restrito :: notícias')

@section('content_header')
    <h1>Lista de notícias</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('noticias.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Título da notícia</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($noticias as $noticia)
                                <tr>
                                    <td>
                                        <a href="{{ route('noticias.edit', $noticia->id) }}" class="btn btn-default btn-sm">Editar</a>
                                    </td>        
                                    <td>{{ $noticia->titulo_noticia }}</td>
                                @empty
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                            {{ $noticias->links() }}
                </div>
            </div>
        </div>
    </div>
@stop