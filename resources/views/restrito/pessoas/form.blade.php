@extends('adminlte::page')
@section('title', 'Restrito :: Pessoas')

@section('content_header')
    <h1>Formulario de cadastro de pessoas</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($pessoa))
                        {!! Form::model($pessoa,['method' => 'PATCH','route' => ['pessoas.update',$pessoa->id], 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['route' => 'pessoas.store', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('nome', 'Nome') !!}
                            {!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Nome completo']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('cargo', 'Bairro') !!}
                            {!! Form::select('cargo', $cargos, null, ['class' => 'form-control', 'placeholder' => 'Selecione cargo...']) !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('ubs_id', 'UBS') !!}
                            {!! Form::select('ubs_id', $ubs, null, ['class' => 'form-control', 'placeholder' => 'Selecione a UBS...']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('imagem', 'Imagem') !!}
                            {!! Form::file('imagem', null, ['class' => 'form-control']) !!}
                            @if (isset($pessoa))
                                <img class="img img-responsive" src="{{ asset('uploads/'.$pessoa->imagem) }}" alt="">
                            @endif
                        </div>
                        
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop