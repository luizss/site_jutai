@extends('adminlte::page')
@section('title', 'Restrito :: pessoas')

@section('content_header')
    <h1>Lista de Pessoas</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('pessoas.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Nome</th>
                                <th>Cargo</th>
                                <th>UBS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($pessoas as $pessoa)
                                <tr>
                                    <td>
                                        <a href="{{ route('pessoas.edit', $pessoa->id) }}" class="btn btn-default btn-sm">Editar</a>
                                    </td>        
                                    <td>{{ $pessoa->nome }}</td>
                                    <td>{{ $pessoa->cargo }}</td>
                                    <td>{{ $pessoa->ubs->nome_ubs }}</td>
                                @empty
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $pessoas->links() }}
                </div>
            </div>
        </div>
    </div>
@stop