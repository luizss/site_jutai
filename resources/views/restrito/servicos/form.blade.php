@extends('adminlte::page')
@section('title', 'Restrito :: Serviço')

@section('content_header')
    <h1>Formulario de cadastro de serviços oferecidos</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($servico))
                        {!! Form::model($servico,['method' => 'PATCH','route' => ['servicos.update',$servico->id], 'enctype' => 'multipart/form-data']) !!}
                    @else
                        {!! Form::open(['route' => 'servicos.store', 'enctype' => 'multipart/form-data']) !!}
                    @endif
                        <div class="form-group">
                            {!! Form::label('nome', 'Nome do serviço') !!}
                            {!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Nome do serviço']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('descricao', 'Descrição do serviço') !!}
                            {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Salvar', ['class' => 'btn btn-success btn-sm']) !!}
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        window.onload = function () {
            CKEDITOR.replace('descricao');
        }
    </script>
@endpush