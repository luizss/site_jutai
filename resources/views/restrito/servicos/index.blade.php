@extends('adminlte::page')
@section('title', 'Restrito :: Serviços')

@section('content_header')
    <h1>Lista de serviços</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('servicos.create') }}" class="btn btn-primary btn-sm">Novo registro</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-responsive datatable">
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Nome do serviço</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($servicos as $servico)
                                <tr>
                                    <td>
                                        <a href="{{ route('servicos.edit', $servico->id) }}" class="btn btn-default btn-sm">Editar</a>
                                    </td>        
                                    <td>{{ $servico->nome }}</td>
                                @empty
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $servicos->links() }}
                </div>
            </div>
        </div>
    </div>
@stop