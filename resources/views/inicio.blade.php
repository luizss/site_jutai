@extends('adminlte::login')
@section('body')
    <div class="login-box">
        <div class="login-box-body">
            {!! Form::open(['route' => 'login.usuario']) !!}
            <div class="form-group">
                {!! Form::input('text', 'usuario', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
            </div>
            <div class="form-group">
                {!! Form::input('password', 'senha', null, ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Entrar',['class' => 'btn btn-primary btn-block btn-flat']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection