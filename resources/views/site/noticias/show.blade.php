@extends('layouts.app')

@section('content')
    <img class="image-carousel" src="{{ asset('uploads/'.$noticia->image_noticia) }}" alt="">
    <div class="container">
        <div class="row">
            <div class="carousel-caption d-block d-md-block">
                <h1 id="titulo-noticia">
                    {{ $noticia->titulo_noticia }}
                </h1>
            </div>
        </div>
        <div class="row">
            <div id="conteudo-noticia" class="col-md-10 offset-md-1">
                <p id="updated_at"> 
                    <small>
                        Atualizado em: {{ $noticia->updated_at->format('d/m/Y H:m:s') }}
                    </small>
                </p>    
                {!! $noticia->conteudo_noticia !!}
            </div>
        </div>
    </div>
    @include('layouts.footer')
@endsection