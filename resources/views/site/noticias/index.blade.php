@extends('layouts.app')

@section('content')
<div class="container" id="todas-noticias">
    <div class="row">
        <div class="col-md-12 titulo-pagina-noticias">
            <h2 class="text-center">Confira as notícias mais recentes aqui!</h2>
        </div>
    </div>
@foreach ($noticias as $noticia)
    <div class="row row-noticias">
        <div class="col-md-4">
            <img class="card-img-top" id="img-pagina-noticias" src="{{ asset('uploads/'.$noticia->image_noticia) }}" alt="">
        </div>
        <div class="col-md-8">
            <b>{{ $noticia->titulo_noticia }}</b>
            <p id="updated_at"> 
                <small>
                    Publicada em: {{ $noticia->created_at->format('d/m/Y H:m:s') }}
                </small>
            </p>
            <p>
                {!! substr($noticia->conteudo_noticia, 0, 250) !!}...
            </p>
            <a href="{{ route('noticia.show', $noticia->url) }}" class="btn btn-primary"><i class="fas fa-eye"></i> Continue lendo</a>    
        </div>
    <hr>
    </div>        

@endforeach
@include('layouts.paginacao')
</div>
@include('layouts.footer')
@endsection