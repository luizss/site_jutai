@extends('layouts.app')

@section('content')
    <div class="container">
        <div id="titulo-pagina-ubs" class="row">
            <div class="col-md-6 offset-md-3">
                <h2 id="titulo" class="text-center">Busque a Unidade que deseja visualizar as informações</h2>
                    <div class="form-group">
                        {!! Form::text('buscaUnidade', null, ['class' => 'form-control', 'placeholder' => 'Informe o nome da unidade', 'id' => 'buscaUnidade']) !!}
                    </div>
                    <div id="result"></div>

                    @include('site.unidades.modal-detalhes')
            </div>
        </div>
    </div>
    <script>
        $("#buscaUnidade").keyup(function () {
            let pesquisa = $("#buscaUnidade").val();
            if (pesquisa != '') {
                let dados_unidades = '';
                $.ajax({
                    url: 'search/'+pesquisa,
                    type: 'GET',
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (response) {
                        $('#result').empty()

                        if (Object.keys(response).length > 0) {
                            dados_unidades += '<p><i class="fa fa-info-circle text-success"></i> Resultado(s) encontrado(s): ' + Object.keys(response).length + '</p>';
                            
                            dados_unidades += '<table class="table table-hover">';
                            dados_unidades += '<tbody>';
                                
                                $.each(response, function (key, value) {
                                    dados_unidades += '<tr>'

                                    dados_unidades += '<th class="table-search" scope="row"><a class="link-info_unidade" data-toggle="modal" data-target="#unidade" data-unidade="'+ value.url+'">' + value.nome_ubs + '</a></th>';
                                });

                            dados_unidades += '</table>';
                            dados_unidades += '</tbody>';

                            $("#result").append(dados_unidades);
                        } else {
                            $('#result').empty();
                            
                            dados_unidades += '<p><i class="fas fa-exclamation-circle text-danger"></i> Nenhum resultado encontrado. Por favor, verifique se o nome está correto.</p>';
                            $("#result").append(dados_unidades);
                        }
                    }, 
                    error: function () {

                    }
                })
            } else {
                $('#result').empty();
            } 
        });

        $('#unidade').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) 
          var url_unidade = button.data('unidade') 
          var modal = $(this);
          var dados_servicos = '';
          var dados_funcionarios = '';
          var dados_unidade = '';
          $('#funcionarios').empty();
          $('#servicos').empty();
          $('#dados-unidade').empty();

          $.ajax({
                  url : "unidade/"+url_unidade,
                  type : 'GET',
                  contentType: "application/json",
                  dataType: "json",
                  success: function(response){
                    
                    document.getElementById('unidadeLabel').innerHTML = '<i class="fas fa-info-circle"></i> '+response.unidade.nome_ubs;
                    
                    dados_unidade += '<h4><i class="fas fa-map-marker-alt"></i> Endereço:</h4>'; 
                    dados_unidade += '<p>'+ response.rua.nome_rua + ', ' + response.unidade.numero + ' - ' + response.bairro + '</p>'
                    $('#dados-unidade').append(dados_unidade);


                    if (Object.keys(response.funcionarios).length > 0) {
                      dados_funcionarios += '<h4><i class="fas fa-users"></i> Funcionários da unidade</h4>';
                    }
                    
                    $.each(response.funcionarios, function(key, value){
                       dados_funcionarios += '<p>' + value.nome + ': ' + value.cargo + '</p>'  
                    });
                    
                    $('#funcionarios').append(dados_funcionarios);

                    if (Object.keys(response.servicos).length > 0) {
                      dados_servicos += '<h4><i class="fas fa-user-md"></i> Serviços disponíveis</h4>';
                    } 
                    
                    $.each(response.servicos, function( chave, valor ) {
                        dados_servicos += '<p><b>' + valor.nome + '</b> ' + valor.descricao + '</p>';
                    });
                    
                    $('#servicos').append(dados_servicos);
                  },
                  error: function() {
                      
                  }
             });
        })
    </script>
@stop