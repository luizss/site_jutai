<div class="modal fade" id="unidade" tabindex="-1" role="dialog" aria-labelledby="unidadeLabel" aria-hidden="true">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="unidadeLabel"></h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="dados-unidade"></div>
                            <div id="funcionarios"></div>
                            <div id="servicos"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
