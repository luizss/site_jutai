<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbsTable extends Migration
{
    public function up()
    {
        Schema::create('ubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_ubs');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ubs');
    }
}
