<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ubs', function (Blueprint $table) {
            $table->string('exibir_tela_inicial',1)->default('s');
            $table->string('url');
            $table->integer('rua_id')->unsigned();
            $table->string('imagem')->nullable();
            $table->foreign('rua_id')->references('id')->on('ruas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ubs', function (Blueprint $table) {
            $table->dropColumn('exibir_tela_inicial');
            $table->dropColumn('url');
            $table->dropColumn('rua_id');
            $table->dropColumn('imagem');
        });
    }
}
