<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbsServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubs_servicos', function (Blueprint $table) {
            // $table->increments('id');
            // $table->timestamps();
            $table->integer('ubs_id')->unsigned();
            $table->integer('servico_id')->unsigned();
            $table->foreign('ubs_id')->references('id')->on('ubs')->onDelete('cascade');
            $table->foreign('servico_id')->references('id')->on('servicos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubs_servicos');
    }
}
