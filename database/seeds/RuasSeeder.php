<?php

use Illuminate\Database\Seeder;
use App\Models\Rua;
use App\Models\Bairro;

class RuasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	$bairro = Bairro::first();

     	Rua::create([
     		'nome_rua' => 'Rua fake',
     		'bairro_id' => $bairro->id
     	]);   
    }
}
