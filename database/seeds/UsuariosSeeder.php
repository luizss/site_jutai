<?php

use Illuminate\Database\Seeder;
use App\Models\Usuario;
class UsuariosSeeder extends Seeder
{
    public function run()
    {
        Usuario::create([
            'nome' => 'Luiz Souza',
            'usuario' => 'admin',
            'senha' => hash('sha512','admin')
        ]);
    }
}
