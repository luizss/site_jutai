<?php

use Illuminate\Database\Seeder;
use App\Models\Bairro;

class BairrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bairro::create([
        	'nome_bairro' => 'Bairro fake'
        ]);
    }
}
