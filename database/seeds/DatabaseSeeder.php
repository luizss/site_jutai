<?php

use Illuminate\Database\Seeder;
use App\Models\Usuario;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuariosSeeder::class);
        $this->call(BairrosSeeder::class);
        $this->call(RuasSeeder::class);
        $this->call(SecretariaSeeder::class);
    }
}
