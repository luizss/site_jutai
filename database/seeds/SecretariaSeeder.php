<?php

use Illuminate\Database\Seeder;
use App\Models\Ubs;
use App\Models\Rua;

class SecretariaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rua = Rua::first();
        Ubs::create([
            'nome_ubs' => 'Secretaria Municipal de Saúde',
            'exibir_tela_inicial' => 'n',
            'url' => 'secretaria-municipal-de-saude',
            'rua_id' => $rua->id,
            'imagem' => null,
            'numero' => null,
        ]);
    }
}
